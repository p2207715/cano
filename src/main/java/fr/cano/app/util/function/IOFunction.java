
package fr.cano.app.util.function;

import java.io.IOException;

@FunctionalInterface
public interface IOFunction<I, O> {

    O apply(I i) throws IOException;

}