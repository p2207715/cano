package fr.cano.app.util;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public final class HttpUtil {

    private HttpUtil() throws IllegalAccessException {
        throw new IllegalAccessException("You cannot instantiate a utility class.");
    }

    /**
     * Read completely an {@link InputStream} and convert into {@link String}
     *
     * @param inputStream The input
     *
     * @return The output {@link String}
     *
     * @throws IOException I/O exception
     */
    public static String readResponse(@NotNull InputStream inputStream) throws IOException {
        Objects.requireNonNull(inputStream);

        final StringBuilder builder = new StringBuilder();

        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        }
        return builder.toString();
    }

    public static URL buildUrl(@NotNull String url) {
        Objects.requireNonNull(url);

        try {
            return URI.create(url).toURL();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

}