
package fr.cano.app.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public final class ExecutorUtil {

    private ExecutorUtil() throws IllegalAccessException {
        throw new IllegalAccessException("You cannot instantiate a utility class.");
    }

    /**
     * Shutdown correctly a {@link ExecutorService} with logging
     *
     * @param id       The executor identifier
     * @param executor The {@link ExecutorService} to shut down
     * @param logger   The {@link Logger} used for problem notifying
     */
    public static void cleanShutdown(@NotNull String id, @Nullable ExecutorService executor, @NotNull Logger logger) {
        Objects.requireNonNull(id);
        Objects.requireNonNull(logger);

        if (executor != null) {
            executor.shutdown();

            try {
                if (!executor.awaitTermination(10, TimeUnit.SECONDS)) {
                    logger.error("The \"{}\" executor could not be closed!", id);
                }
            } catch (InterruptedException e) {
                logger.error("An exception was thrown during \"{}\"executor shutting down", id, e);
            }
        }
    }

}