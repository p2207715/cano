package fr.cano.app.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CanoConfiguration {

    @Value("${cano.llm.apiKey}")
    private String llmApiKey;

    @Value("${cano.llm.apiUrl}")
    private String llmApiUrl;

    public String getLlmApiKey() {
        return this.llmApiKey;
    }

    public String getLlmApiUrl() {
        return this.llmApiUrl;
    }

}
