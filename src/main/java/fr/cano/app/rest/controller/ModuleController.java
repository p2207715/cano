package fr.cano.app.rest.controller;

import fr.cano.app.mail.module.Module;
import fr.cano.app.mail.module.ModuleType;
import fr.cano.app.mail.module.impl.AntiSpamModule;
import fr.cano.app.mail.module.impl.AutoMoveModule;
import fr.cano.app.mail.module.impl.AutoReplyModule;
import fr.cano.app.user.User;
import fr.cano.app.user.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.function.Function;

@RestController
@RequestMapping("/api/module")
@CrossOrigin(origins = "http://localhost:3000")
public class ModuleController {

    private final UserService userService;

    public ModuleController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/config/antispam")
    public void updateAntiSpam(@RequestParam String user, @RequestParam String prompt, @RequestParam String folder) {
        final User foundUser = this.userService.getUser(user);

        if (foundUser == null) {
            return;
        }
        foundUser.getModules().compute(ModuleType.ANTI_SPAM, (k, v) -> {
            if (v instanceof AntiSpamModule module) {
                module.setTargetFolder(folder);
                module.setPrompt(prompt);
                return v;
            }
            return new AntiSpamModule(folder, prompt);
        });
    }

    @PostMapping("/config/automove")
    public void updateAutoMove(@RequestParam String user, @RequestParam String prompt, @RequestParam String folder) {
        final User foundUser = this.userService.getUser(user);

        if (foundUser == null) {
            return;
        }
        foundUser.getModules().compute(ModuleType.AUTO_MOVE, (k, v) -> {
            if (v instanceof AutoMoveModule module) {
                module.setTargetFolder(folder);
                module.setPrompt(prompt);
                return v;
            }
            return new AutoMoveModule(folder, prompt);
        });
    }

    @PostMapping("/config/autoreply")
    public void updateAutoReply(@RequestParam String user, @RequestParam String filterPrompt, @RequestParam String replyPrompt) {
        final User foundUser = this.userService.getUser(user);

        if (foundUser == null) {
            return;
        }
        foundUser.getModules().compute(ModuleType.AUTO_REPLY, (k, v) -> {
            if (v instanceof AutoReplyModule module) {
                module.setFilterPrompt(filterPrompt);
                module.setReplyPrompt(replyPrompt);
                return v;
            }
            return new AutoReplyModule(filterPrompt, replyPrompt);
        });
    }

    @GetMapping("/config/antispam")
    public ResponseEntity<Module> getAntiSpam(@RequestParam String user) {
        final User foundUser = this.userService.getUser(user);

        if (foundUser == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(foundUser.getModules().get(ModuleType.ANTI_SPAM));
    }

    @GetMapping("/config/automove")
    public ResponseEntity<Module> getAutoMove(@RequestParam String user) {
        final User foundUser = this.userService.getUser(user);

        if (foundUser == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(foundUser.getModules().get(ModuleType.AUTO_MOVE));
    }

    @GetMapping("/config/autoreply")
    public ResponseEntity<Module> getAutoReply(@RequestParam String user) {
        final User foundUser = this.userService.getUser(user);

        if (foundUser == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(foundUser.getModules().get(ModuleType.AUTO_REPLY));
    }

    @GetMapping("/stats/antispam/process")
    public Count getAntiSpamMessageCount(@RequestParam String user) {
        return new Count(this.getProcessCount(this.userService.getUser(user), ModuleType.ANTI_SPAM));
    }

    @GetMapping("/stats/antispam/flag")
    public Count getAntiSpamFlagCount(@RequestParam String user) {
        return new Count(this.getFlagCount(this.userService.getUser(user), ModuleType.ANTI_SPAM));
    }

    @GetMapping("/stats/automove/process")
    public Count getAutoMoveMessageCount(@RequestParam String user) {
        return new Count(this.getProcessCount(this.userService.getUser(user), ModuleType.AUTO_MOVE));
    }

    @GetMapping("/stats/automove/flag")
    public Count getAutoMoveFlagCount(@RequestParam String user) {
        return new Count(this.getFlagCount(this.userService.getUser(user), ModuleType.AUTO_MOVE));
    }

    @GetMapping("/stats/autoreply/process")
    public Count getAutoReplyMessageCount(@RequestParam String user) {
        return new Count(this.getProcessCount(this.userService.getUser(user), ModuleType.AUTO_REPLY));
    }

    @GetMapping("/stats/autoreply/flag")
    public Count getAutoReplyFlagCount(@RequestParam String user) {
        return new Count(this.getFlagCount(this.userService.getUser(user), ModuleType.AUTO_REPLY));
    }

    private long getFlagCount(User user, ModuleType moduleType) {
        return this.getCount(user, moduleType, Module::getFlaggedMessageCount);
    }

    private long getProcessCount(User user, ModuleType moduleType) {
        return this.getCount(user, moduleType, Module::getProcessedMessageCount);
    }

    private long getCount(User user, ModuleType moduleType, Function<Module, Long> function) {
        if (user == null) {
            return 0;
        }
        final Module module = user.getModules().get(moduleType);

        return module == null ? 0 : function.apply(module);
    }

    public record Count(long count) {
    }

}