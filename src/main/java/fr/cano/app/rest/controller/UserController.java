package fr.cano.app.rest.controller;

import fr.cano.app.mail.MailHost;
import fr.cano.app.user.User;
import fr.cano.app.user.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
@CrossOrigin(origins = "http://localhost:3000")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public boolean login(@RequestBody LoginRequest loginRequest) {
        final User user = this.userService.getUser(loginRequest.mail);
        return user != null && user.getWebsitePassword().equals(loginRequest.password);
    }

    @GetMapping("/get")
    public User get(@RequestParam String mail) {
        return this.userService.getUser(mail);
    }

    @PostMapping("/update")
    public ResponseEntity<?> update(@RequestBody UserUpdate userUpdate) {
        final User oldUser = this.userService.getUser(userUpdate.email);

        if (oldUser == null) {
            return ResponseEntity.badRequest().build();
        }
        oldUser.setMailPassword(userUpdate.mailPassword);
        oldUser.setImapHost(new MailHost(userUpdate.imapHost, userUpdate.imapPort));
        oldUser.setSmtpHost(new MailHost(userUpdate.smtpHost, userUpdate.smtpPort));

        this.userService.saveUser(oldUser);
        return ResponseEntity.ok().build();
    }

    public record LoginRequest(String mail, String password) {

    }

    public record UserUpdate(String email, String mailPassword, String imapHost, int imapPort, String smtpHost, int smtpPort) {

    }

}