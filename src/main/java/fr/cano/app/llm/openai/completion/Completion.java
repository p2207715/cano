package fr.cano.app.llm.openai.completion;

import com.google.gson.annotations.SerializedName;
import fr.cano.app.llm.openai.data.GPTModel;
import fr.cano.app.llm.openai.data.Message;
import org.jetbrains.annotations.NotNull;

public record Completion(@NotNull String id, @NotNull String object, int created, @NotNull GPTModel model,
                         @NotNull Choice[] choices, @NotNull Usage usage) {

    public record Choice(int index, @NotNull Message message,
                         @SerializedName("finish_reason") @NotNull String finishReason) {
    }

    public record Usage(@SerializedName("prompt_tokens") int promptTokens,
                        @SerializedName("completion_tokens") int completionTokens,
                        @SerializedName("total_tokens") int totalTokens) {
    }

}