package fr.cano.app.llm.openai.exception;

import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class OpenAIException extends Exception {

    private Error error;

    public OpenAIException(@NotNull String message) {
        super(message);
    }

    public OpenAIException(@NotNull Error error) {
        this(error.message());
        this.error = error;
    }

    @Nullable
    public Error getError() {
        return this.error;
    }

    public record ErrorWrapper(Error error) {
    }

    public record Error(Code code, String message) {

        public enum Code {
            @SerializedName("content_policy_violation") CONTENT_POLICY_VIOLATION
        }

    }

}