package fr.cano.app.llm.openai.completion;

import com.google.gson.annotations.SerializedName;
import fr.cano.app.llm.openai.data.GPTModel;
import fr.cano.app.llm.openai.data.Message;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public record CompletionRequest(@NotNull GPTModel model, @NotNull Message[] messages, @Nullable Double temperature,
                                @Nullable Integer n, @SerializedName("max_tokens") @Nullable Integer maxTokens,
                                @Nullable String user) {

    public static class Builder {

        private final GPTModel model;
        private final Message[] messages;

        private Double temperature;
        private Integer n;
        private Integer maxTokens;
        private String user;

        public Builder(@NotNull GPTModel model, @NotNull Message... messages) {
            this.model = Objects.requireNonNull(model);
            this.messages = Objects.requireNonNull(messages);
        }

        public Builder(@NotNull Message... messages) {
            this(GPTModel.GPT_4O_MINI, Objects.requireNonNull(messages));
        }

        public Builder temperature(@Nullable Double temperature) {
            this.temperature = temperature;
            return this;
        }

        public Builder n(@Nullable Integer n) {
            this.n = n;
            return this;
        }

        public Builder maxTokens(@Nullable Integer maxTokens) {
            this.maxTokens = maxTokens;
            return this;
        }

        public Builder user(@Nullable String user) {
            this.user = user;
            return this;
        }

        public @NotNull CompletionRequest build() {
            return new CompletionRequest(this.model, this.messages, this.temperature, this.n, this.maxTokens, this.user);
        }

    }

}