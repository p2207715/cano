package fr.cano.app.llm.openai.data;

import com.google.gson.annotations.SerializedName;

public enum Role {
    @SerializedName("system") SYSTEM,
    @SerializedName("user") USER,
    @SerializedName("assistant") ASSISTANT,
    @SerializedName("function") FUNCTION
}