package fr.cano.app.llm.openai.data;

import com.google.gson.annotations.SerializedName;

public enum GPTModel {
    @SerializedName("gpt-4o-mini") GPT_4O_MINI,
    @SerializedName("gpt-4-0125-preview") GPT_4,
    @SerializedName("gpt-3.5-turbo-0125") GPT_3_5_TURBO
}