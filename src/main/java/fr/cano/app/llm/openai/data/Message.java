package fr.cano.app.llm.openai.data;

import org.jetbrains.annotations.NotNull;

public record Message(@NotNull Role role, @NotNull String content) {
}