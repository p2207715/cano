package fr.cano.app.llm;

import com.google.gson.Gson;
import fr.cano.app.configuration.CanoConfiguration;
import fr.cano.app.llm.openai.completion.Completion;
import fr.cano.app.llm.openai.completion.CompletionRequest;
import fr.cano.app.llm.openai.exception.OpenAIException;
import fr.cano.app.util.ExecutorUtil;
import fr.cano.app.util.HttpUtil;
import fr.cano.app.util.function.IOFunction;
import jakarta.annotation.PreDestroy;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class LLMService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LLMService.class);
    private static final int TIMEOUT = 5000;

    private final Gson gson = new Gson();

    private final ExecutorService executor;
    private final String apiKey;
    private final URL url;

    public LLMService(CanoConfiguration configuration) {
        this.executor = Executors.newVirtualThreadPerTaskExecutor();
        this.apiKey = "Bearer " + configuration.getLlmApiKey();
        this.url = HttpUtil.buildUrl(configuration.getLlmApiUrl());
    }

    @PreDestroy
    public void destroy() {
        ExecutorUtil.cleanShutdown("LLMService", this.executor, LOGGER);
    }

    public @NotNull CompletableFuture<Completion> createCompletion(@NotNull CompletionRequest request) {
        return this.createRequest(request, Completion.class, this.url);
    }

    private <I, O> CompletableFuture<O> createRequest(@NotNull I request, @NotNull Class<O> responseClass, @NotNull URL url) {
        return this.createRequest(request, url, inputStream -> this.gson.fromJson(HttpUtil.readResponse(inputStream), responseClass));
    }

    private <I, O> CompletableFuture<O> createRequest(@NotNull I request, @NotNull URL url, @NotNull IOFunction<InputStream, O> parse) {
        final CompletableFuture<O> future = new CompletableFuture<>();

        this.executor.execute(() -> {
            final String jsonData = this.gson.toJson(request);
            HttpURLConnection connection = null;

            try {
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setConnectTimeout(TIMEOUT);
                connection.setReadTimeout(TIMEOUT);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Authorization", this.apiKey);

                connection.setDoOutput(true);

                try (final OutputStream os = connection.getOutputStream()) {
                    final byte[] input = jsonData.getBytes(StandardCharsets.UTF_8);
                    os.write(input, 0, input.length);
                }
                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    try (final InputStream inputStream = connection.getInputStream()) {
                        final O response = parse.apply(inputStream);

                        if (response == null) {
                            future.completeExceptionally(new Exception("Failed to parse response"));
                            return;
                        }
                        future.complete(response);
                        return;
                    }
                }
                future.completeExceptionally(new OpenAIException(this.gson.fromJson(HttpUtil.readResponse(connection.getErrorStream()), OpenAIException.ErrorWrapper.class).error()));
            } catch (IOException e) {
                future.completeExceptionally(new OpenAIException(e.getMessage()));
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        });
        return future;
    }

}
