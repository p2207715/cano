package fr.cano.app.mail;

import com.sun.mail.imap.IMAPFolder;
import jakarta.mail.Folder;
import jakarta.mail.Message;
import jakarta.mail.event.MessageCountAdapter;
import jakarta.mail.event.MessageCountEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;

public class MailListenerThread extends Thread {

    private static final Logger LOGGER = LoggerFactory.getLogger(MailListenerThread.class);

    private final Folder inbox;
    private final Consumer<Message> messageConsumer;

    private volatile boolean running = true;

    public MailListenerThread(Folder inbox, Consumer<Message> messageConsumer) {
        this.inbox = inbox;
        this.messageConsumer = messageConsumer;
    }

    @Override
    public void run() {
        this.inbox.addMessageCountListener(new MessageCountAdapter() {
            @Override
            public void messagesAdded(MessageCountEvent event) {
                for (Message message : event.getMessages()) {
                    MailListenerThread.this.messageConsumer.accept(message);
                }
            }
        });
        while (this.running) {
            try {
                if (!this.inbox.isOpen()) {
                    this.inbox.open(Folder.READ_WRITE);
                }
                ((IMAPFolder) this.inbox).idle();
            } catch (Exception e) {
                LOGGER.error("Error while listening for mails", e);
            }
        }
    }

    public void stopListening() {
        this.running = false;
    }

}
