package fr.cano.app.mail.module.impl;

import fr.cano.app.llm.openai.completion.Completion;
import fr.cano.app.llm.openai.completion.CompletionRequest;
import fr.cano.app.llm.openai.data.Role;
import fr.cano.app.mail.MailService;
import fr.cano.app.mail.module.Module;
import fr.cano.app.mail.module.ModuleType;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;

public class AutoReplyModule extends Module {
    private static final Logger LOGGER = LoggerFactory.getLogger(AutoReplyModule.class);
    private static final fr.cano.app.llm.openai.data.Message FILTER_PRE_PROMPT = new fr.cano.app.llm.openai.data.Message(Role.SYSTEM, "You are used for auto-replying in an e-mail service. Based on the next prompt, do you think you should generate a response to the user's message? Only respond by 'true' or 'false'.");
    private static final fr.cano.app.llm.openai.data.Message REPLY_PRE_PROMPT = new fr.cano.app.llm.openai.data.Message(Role.SYSTEM,
            "You are used for auto-replying in an e-mail service. Based on the next prompt, generate the response from the user's message" +
                    ". Only generate the content, not the subject. Your message will be directly sent so do not put any placeholder.");

    private String filterPrompt;
    private String replyPrompt;

    public AutoReplyModule(String filterPrompt, String replyPrompt) {
        super(ModuleType.AUTO_REPLY);
        this.filterPrompt = filterPrompt;
        this.replyPrompt = replyPrompt;
    }

    @Override
    public void handle(MailService mailService, Message message) {
        final String content = this.getContent(message, LOGGER);

        if (content == null) {
            return;
        }
        final CompletableFuture<Completion> filterFuture = mailService.getLlmService().createCompletion(
                new CompletionRequest.Builder(
                        FILTER_PRE_PROMPT,
                        this.buildLLMMessageSystem(this.filterPrompt),
                        this.buildLLMMessageUser(content)
                ).build()
        );

        filterFuture.whenComplete((completion, throwable) -> {
            if (throwable != null) {
                LOGGER.error("An error occurred while processing email", throwable);
                return;
            }
            final boolean flag = Boolean.parseBoolean(completion.choices()[0].message().content());

            LOGGER.info("Email should be replied: {}", flag);

            if (flag) {
                this.increaseFlagMessageCount();

                final CompletableFuture<Completion> replyFuture = mailService.getLlmService().createCompletion(
                        new CompletionRequest.Builder(
                                REPLY_PRE_PROMPT,
                                this.buildLLMMessageSystem(this.replyPrompt),
                                this.buildLLMMessageUser(content)
                        ).build()
                );

                replyFuture.whenComplete((replyCompletion, replyThrowable) -> {
                    if (replyThrowable != null) {
                        LOGGER.error("An error occurred while processing email", replyThrowable);
                        return;
                    }
                    final String replyContent = replyCompletion.choices()[0].message().content();

                    LOGGER.info("Reply content: {}", replyContent);

                    try {
                        mailService.replyEmail(message, replyContent);
                    } catch (MessagingException e) {
                        LOGGER.error("An error occurred while replying email", e);
                    }
                });

            }
        });
    }

    public void setFilterPrompt(String filterPrompt) {
        this.filterPrompt = filterPrompt;
    }

    public void setReplyPrompt(String replyPrompt) {
        this.replyPrompt = replyPrompt;
    }

}