package fr.cano.app.mail.module;

import fr.cano.app.mail.MailService;
import jakarta.mail.Message;
import org.slf4j.Logger;

public abstract class Module {

    private final ModuleType type;

    private long processedMessageCount;
    private long flaggedMessageCount;

    protected Module(ModuleType type) {
        this.type = type;
    }

    public abstract void handle(MailService mailService, Message message);

    protected void increaseFlagMessageCount() {
        this.flaggedMessageCount++;
    }

    protected String getContent(Message message, Logger logger) {
        try {
            return MailService.getTextFromMimeMultipart((jakarta.mail.internet.MimeMultipart) message.getContent());
        } catch (Exception e) {
            logger.error("An error occurred while processing email", e);
            return null;
        } finally {
            this.processedMessageCount++;
        }
    }

    protected fr.cano.app.llm.openai.data.Message buildLLMMessageSystem(String content) {
        return new fr.cano.app.llm.openai.data.Message(fr.cano.app.llm.openai.data.Role.SYSTEM, content);
    }

    protected fr.cano.app.llm.openai.data.Message buildLLMMessageUser(String content) {
        return new fr.cano.app.llm.openai.data.Message(fr.cano.app.llm.openai.data.Role.USER, content);
    }

    public long getProcessedMessageCount() {
        return this.processedMessageCount;
    }

    public long getFlaggedMessageCount() {
        return this.flaggedMessageCount;
    }

    public ModuleType getType() {
        return this.type;
    }

}