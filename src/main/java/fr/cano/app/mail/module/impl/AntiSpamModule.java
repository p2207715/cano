package fr.cano.app.mail.module.impl;

import fr.cano.app.llm.openai.completion.Completion;
import fr.cano.app.llm.openai.completion.CompletionRequest;
import fr.cano.app.llm.openai.data.Role;
import fr.cano.app.mail.MailService;
import fr.cano.app.mail.module.Module;
import fr.cano.app.mail.module.ModuleType;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;

// L'AntiSpam est très semblable à l'AutoMove, on aurait donc pu utiliser une classe abstraite pour éviter la duplication de code mais on
// suppose qu'elle pourrait évoluer différemment dans le futur (ex: au lieu de déplacer les mails dans un dossier, ils sont directement
// supprimés).
public class AntiSpamModule extends Module {

    private static final Logger LOGGER = LoggerFactory.getLogger(AntiSpamModule.class);
    private static final fr.cano.app.llm.openai.data.Message PRE_PROMPT = new fr.cano.app.llm.openai.data.Message(Role.SYSTEM, "You are used for detecting spams in an e-mail service. Based on the next prompt, do you think the message of the user is a spam? Only respond by 'true' or 'false'.");

    private String targetFolder;
    private String prompt;

    public AntiSpamModule(String targetFolder, String prompt) {
        super(ModuleType.ANTI_SPAM);
        this.targetFolder = targetFolder;
        this.prompt = prompt;
    }

    @Override
    public void handle(MailService mailService, Message message) {
        final String content = this.getContent(message, LOGGER);

        if (content == null) {
            return;
        }
        final CompletableFuture<Completion> future = mailService.getLlmService().createCompletion(
                new CompletionRequest.Builder(
                        PRE_PROMPT,
                        this.buildLLMMessageSystem(this.prompt),
                        this.buildLLMMessageUser(content)
                ).build()
        );

        future.whenComplete((completion, throwable) -> {
            if (throwable != null) {
                LOGGER.error("An error occurred while processing email", throwable);
                return;
            }
            final boolean flag = Boolean.parseBoolean(completion.choices()[0].message().content());

            if (flag) {
                this.increaseFlagMessageCount();

                LOGGER.info("Email is a spam, moving to folder: {}", this.targetFolder);

                try {
                    mailService.moveMessageToFolder(message.getFolder().getStore(), message, this.targetFolder);
                } catch (MessagingException e) {
                    LOGGER.error("An error occurred while moving email", e);
                }
            }
        });
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public void setTargetFolder(String targetFolder) {
        this.targetFolder = targetFolder;
    }

}