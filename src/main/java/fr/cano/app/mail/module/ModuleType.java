package fr.cano.app.mail.module;

public enum ModuleType {
    ANTI_SPAM,
    AUTO_MOVE,
    AUTO_REPLY
}
