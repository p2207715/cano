package fr.cano.app.mail;

public record MailHost(String host, int port) {

}
