package fr.cano.app.mail;

import fr.cano.app.llm.LLMService;
import fr.cano.app.mail.module.Module;
import fr.cano.app.user.User;
import fr.cano.app.user.UserService;
import fr.cano.app.util.ExecutorUtil;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.mail.*;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

@Service
public class MailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MailService.class);

    private final LLMService llmService;
    private final UserService userService;

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    public MailService(LLMService llmService, UserService userService) {
        this.llmService = llmService;
        this.userService = userService;
    }

    @PostConstruct
    public void init() throws MessagingException {
        for (User user : this.userService.getAllUsers()) {
            if (user.getImapHost() == null) {
                continue;
            }
            this.listenForNewEmails(user.getImapHost(), user.getMail(), user.getMailPassword(), message -> {
                try {
                    LOGGER.info("Received new email from: {}", message.getFrom()[0]);

                    for (Module module : user.getModules().values()) {
                        module.handle(this, message);
                    }
                } catch (Exception e) {
                    LOGGER.error("An error occurred while reading email", e);
                }
            });
        }
    }

    @PreDestroy
    public void destroy() {
        ExecutorUtil.cleanShutdown("MailService", this.executorService, LOGGER);
    }

    public Session createMailSession(MailHost mailHost, String username, String password, String protocol) {
        final Properties properties = new Properties();
        properties.put("mail." + protocol + ".host", mailHost.host());
        properties.put("mail." + protocol + ".port", String.valueOf(mailHost.port()));
        properties.put("mail." + protocol + ".auth", "true");
        properties.put("mail." + protocol + ".ssl.enable", "true"); // SSL
        properties.put("mail." + protocol + ".starttls.enable", "true"); // TLS
        //properties.put("mail.debug", "true");

        final Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        };
        return Session.getInstance(properties, authenticator);
    }

    public void moveMessageToFolder(Store store, Message message, String destinationFolderName) throws MessagingException {
        final Folder destinationFolder = store.getFolder(destinationFolderName);

        if (!destinationFolder.exists()) {
            destinationFolder.create(Folder.HOLDS_MESSAGES);
        }

        if (!destinationFolder.isOpen()) {
            destinationFolder.open(Folder.READ_WRITE);
        }

        final Folder sourceFolder = message.getFolder();

        if (!sourceFolder.isOpen()) {
            sourceFolder.open(Folder.READ_WRITE);
        }

        sourceFolder.copyMessages(new Message[]{message}, destinationFolder);

        message.setFlag(Flags.Flag.DELETED, true);

        //sourceFolder.expunge();
        destinationFolder.close(false);
    }


    public void replyEmail(Message message, String body) throws MessagingException {
        final InternetAddress recipientAddress = (InternetAddress) message.getRecipients(Message.RecipientType.TO)[0];
        final String email = recipientAddress.getAddress();

        final User user = this.userService.getUser(email);
        final Session session = this.createMailSession(user.getSmtpHost(), user.getMail(), user.getMailPassword(), "smtp");

        final MimeMessage replyMessage = (MimeMessage) message.reply(false);
        replyMessage.setFrom(new InternetAddress(user.getMail()));
        replyMessage.setText(body);
        replyMessage.setSubject("Re: " + message.getSubject());
        replyMessage.setSentDate(new Date());

        try (final Transport transport = session.getTransport("smtp")) {
            transport.connect();
            transport.sendMessage(replyMessage, replyMessage.getAllRecipients());
        } catch (MessagingException e) {
            LOGGER.error("Erreur lors de l'envoi de la réponse à l'email : {}", e.getMessage(), e);
            throw e;
        }
    }


    private void listenForNewEmails(MailHost imapHost, String username, String password, Consumer<Message> messageConsumer) throws MessagingException {
        final Session session = this.createMailSession(imapHost, username, password, "imap");

        final Store store = session.getStore("imap");
        store.connect();

        final Folder inbox = store.getFolder("INBOX");
        inbox.open(Folder.READ_WRITE);

        this.executorService.execute(new MailListenerThread(inbox, messageConsumer));
    }

    public LLMService getLlmService() {
        return this.llmService;
    }

    public static String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws Exception {
        final StringBuilder result = new StringBuilder();

        for (int i = 0; i < mimeMultipart.getCount(); i++) {
            final BodyPart bodyPart = mimeMultipart.getBodyPart(i);

            if (bodyPart.isMimeType("text/plain")) {
                result.append(bodyPart.getContent());
            } else if (bodyPart.isMimeType("text/html")) {
                result.append(bodyPart.getContent()); // Si HTML, l'ajouter
            } else if (bodyPart.getContent() instanceof MimeMultipart) {
                result.append(getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent()));
            }
        }
        return result.toString();
    }

}