package fr.cano.app.user;

import fr.cano.app.mail.MailHost;
import fr.cano.app.mail.module.Module;
import fr.cano.app.mail.module.ModuleType;

import java.util.EnumMap;
import java.util.Map;

public class User {

    private final String mail;
    private String mailPassword;

    private final String websitePassword;

    private MailHost imapHost;
    private MailHost smtpHost;

    private final Map<ModuleType, Module> modules = new EnumMap<>(ModuleType.class);

    public User(String mail, String mailPassword, String websitePassword, MailHost imapHost, MailHost smtpHost) {
        this.mail = mail;
        this.mailPassword = mailPassword;
        this.websitePassword = websitePassword;
        this.imapHost = imapHost;
        this.smtpHost = smtpHost;
    }

    public String getMail() {
        return this.mail;
    }

    public String getMailPassword() {
        return this.mailPassword;
    }

    public String getWebsitePassword() {
        return this.websitePassword;
    }

    public MailHost getImapHost() {
        return this.imapHost;
    }

    public MailHost getSmtpHost() {
        return this.smtpHost;
    }

    public void setMailPassword(String mailPassword) {
        this.mailPassword = mailPassword;
    }

    public void setImapHost(MailHost imapHost) {
        this.imapHost = imapHost;
    }

    public void setSmtpHost(MailHost smtpHost) {
        this.smtpHost = smtpHost;
    }

    public Map<ModuleType, Module> getModules() {
        return this.modules;
    }

}