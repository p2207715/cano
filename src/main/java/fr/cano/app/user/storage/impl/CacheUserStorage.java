package fr.cano.app.user.storage.impl;

import fr.cano.app.user.User;
import fr.cano.app.user.storage.UserStorage;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class CacheUserStorage implements UserStorage {

    private final Map<String, User> users = new ConcurrentHashMap<>();

    @Override
    public User getUser(String mail) {
        return this.users.get(mail);
    }

    @Override
    public void saveUser(User user) {
        this.users.put(user.getMail(), user);
    }

    @Override
    public Collection<User> getAllUsers() {
        return this.users.values();
    }

}