package fr.cano.app.user.storage;

import fr.cano.app.user.User;

import java.util.Collection;

public interface UserStorage {

    User getUser(String mail);

    void saveUser(User user);

    Collection<User> getAllUsers();

}
