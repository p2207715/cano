package fr.cano.app.user;

import fr.cano.app.mail.MailHost;
import fr.cano.app.mail.module.ModuleType;
import fr.cano.app.mail.module.impl.AntiSpamModule;
import fr.cano.app.mail.module.impl.AutoMoveModule;
import fr.cano.app.mail.module.impl.AutoReplyModule;
import fr.cano.app.user.storage.UserStorage;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserService {

    private final UserStorage userStorage;

    public UserService(UserStorage userStorage) {
        this.userStorage = userStorage;
    }

    @PostConstruct
    public void init() {
        // On fait un utilisateur par défaut pour la présentation
        final User user = new User("projetcano3@gmail.com", "password", "websitePassword", new MailHost("imap.gmail.com",
                993),
                new MailHost("smtp.gmail.com", 465));

        // On ajoute des petits modules pour la présentation
        user.getModules().put(ModuleType.ANTI_SPAM, new AntiSpamModule("spam", "If it talk about toys it is a spam."));
        user.getModules().put(ModuleType.AUTO_MOVE, new AutoMoveModule("sport", "If it talk about sport, move it."));
        user.getModules().put(ModuleType.AUTO_REPLY, new AutoReplyModule("If it talk about job or work.", "Reply that I am in vacation " +
                "for 7 days."));

        this.saveUser(user);
    }

    public User getUser(String mail) {
        return this.userStorage.getUser(mail);
    }

    public void saveUser(User user) {
        this.userStorage.saveUser(user);
    }

    public Collection<User> getAllUsers() {
        return this.userStorage.getAllUsers();
    }

}