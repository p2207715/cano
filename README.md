# 🌐 CANO - BackEnd

CANO est un logiciel cloud développé en Java, accompagné d'une interface WEB développé avec React.js et Tailwind CSS.

## 🚀 Installation

1. Cloner le repository
2. Ouvrir le projet dans votre IDE
3. Configurer le fichier `application.properties`
4. Exécuter le projet, la classe main se trouve à `fr.cano.app.main.Main`
5. Le serveur sera accessible à l'adresse suivante : `http://localhost:8080` (ou autre port si vous le changez)
6. Lancez l'interface web pour configurer l'utilisateur

## 📋 Pré-requis

- Java 21 ou plus

## 💼 Changements requis pour un usage "commercial/prod"

- Implémenter une base de donnée persistante
- Implémenter un système d'authentification des requêtes

## ✨ Améliorations possibles

- Implémenter un système de logs
- Implémenter un système de recherche de mail avec IA
- Pouvoir avoir autant de modules (AutoReply, AntiSpam, AutoMove) que souhaité et pas seulement 1.

## 🎨 Front-End

Le repository pour l'interface web se trouve [ici](https://forge.univ-lyon1.fr/p2310055/cano_interface).



